This document contains instructions to build and install Wireshark on Windows
using GCC and MinGW-w64 with the MSYS2 distribution.

How to setup MSYS2:

1. Download and install MSYS2 from https://www.msys2.org.

2. Open the MSYS shell.

3. Update with "pacman -Syu" as many times as necessary. You also need to
   have base-devel installed:
   
    $ pacman -Syu base-devel.

How to build the Wireshark MSYS2 package:

1. Open the shell for the selected environment. If in doubt choose the UCRT64 shell.

2. Build and install Wireshark using the MSYS2 PKGBUILD:

    $ cp -r $srcdir/packaging/msys2/mingw-w64-wireshark-git ./$builddir
    $ cd $builddir
    $ makepkg-mingw -si

The application should be launched using the same shell.

Currently the MSYS2 build has the following limitations compared to
the build using Visual Studio (MSVC):

* The Event Tracing for Windows (ETW) extcap cannot be compiled using MinGW-w64.

* Lua version is 5.1 (MSVC uses Lua 5.2) and does not have UTF-8 patches[1]. 

* Enhanced Kerberos dissection with decryption is not available.

* AirPcap SDK is not available. AirPcap is EOL and currently there is no plan to
  add support for it with MSYS2 builds.

* TODO: Add a stand-alone distributable binary installer that can be
  deployed outside the MSYS2 environment.

References:

[1]https://github.com/Lekensteyn/lua-unicode
